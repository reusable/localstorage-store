var bluebird = require('bluebird')

module.exports = {
    set: function (key, value) {
        console.log('setting on local storage ', key, value)
        localStorage.setItem(key, value)
        return bluebird.resolve(value)
    },
    get: function (key) {
        console.log('getting from local storage ', key, localStorage.getItem(key))
        return bluebird.resolve(localStorage.getItem(key))
    }
}